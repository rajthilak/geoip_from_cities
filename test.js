const   RioGeo   = require('./lib/index.js');

const G =  new RioGeo({});

console.log("1. LIVE Delhi, IN");
const t = G.fillWithGeoInfo("Delhi", "IN");
console.log(JSON.stringify(t));

console.log("\n2. CACHE Delhi, IN");
const x = G.fillWithGeoInfo("Delhi", "IN");
console.log(JSON.stringify(x));

console.log("\n3. INVALID COUNTRY New York, USA");
const u = G.fillWithGeoInfo("New York", "USA");
console.log(JSON.stringify(u));

console.log("\n4. LIVE COUNTRY New York, US");
const v = G.fillWithGeoInfo("New York", "US");
console.log(JSON.stringify(v));

console.log("-----------------------------");

console.log("\n5. LIVE[oNLY] COUNTRY New Y, US");
const w = G.locateCity("New Y", "US");
console.log(JSON.stringify(w));

console.log("\n6. LIVE[oNLY] COUNTRY New York, US");
const z = G.locateCity("New York", "US");
console.log(JSON.stringify(z));


console.log("\nr7. LIVE[oNLY] COUNTRY Toki, JP");
const a = G.locateCity("Toki", "JP");
console.log(JSON.stringify(a));


console.log("\nr8. LIVE[oNLY] COUNTRY Del, IN");
const b = G.locateCity("Del", "IN");
console.log(JSON.stringify(b));
