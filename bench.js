//////////// Benchmark tEST /////////////////////


let bench = require('nanobench');
const   RioGeo   = require('./lib/index.js');

const G =  new RioGeo({});


bench('DELHI IN 100 times', function (b) {
  b.start()

  for (var i = 0; i < 100; i++) {
    data = G.fillWithGeoInfo("Delhi", "IN");
  }

  b.end()
})



bench('New York USA 100 times', function (b) {
  b.start()

  for (var i = 0; i < 100; i++) {
    data = G.fillWithGeoInfo("New York", "USA");
  }

  b.end()
})



bench('New York US 100 times', function (b) {
  b.start()

  for (var i = 0; i < 100; i++) {
    data = G.fillWithGeoInfo("New York", "US");
  }

  b.end()
}) 

