# geoip_from_cities

Figure out the latittude and longitude of a city.

Doesn't use the error prone google geoip lookup.

# How to use this npm 

```

const search = require('geoip_from_cities');

### The country name must be the ISO format 
### The result is cached.
const s = search('Delhi', 'IN');

```

Refer test.js for more test cases. 

This npm uses the cities.json and groups the cites + countries.

The results are cached.

To get a live result 


```

const search = require('geoip_from_cities');

### The country name must be the ISO format 
### The result is cached.
const s = search('Delhi', 'IN', true);

```

# License

MIT
